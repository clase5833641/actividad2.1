


using UnityEngine;



public class PlayerFPSController : MonoBehaviour
{
    public GameObject cam;
    [SerializeField] float walkSpeed = 5f;
    [SerializeField] float hRotationSpeed = 100f;
    [SerializeField] float vRotationSpeed = 80f;
    void Start()
    {
        GameObject.Find("DarthVader").gameObject.SetActive(false);

        Cursor.visible = false;

        Cursor.lockState = CursorLockMode.Locked;

    }
    void Update()
    {

        Movement();
    }

    void Movement()
    {

        
        Vector3 movementDirection = Input.GetAxisRaw("Horizontal") * Vector3.right + Input.GetAxisRaw("Vertical") * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));

        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;
        transform.Rotate(0f, hPlayerRotation, 0f);
        cam.transform.Rotate(-vCamRotation, 0f, 0f);
    }
}
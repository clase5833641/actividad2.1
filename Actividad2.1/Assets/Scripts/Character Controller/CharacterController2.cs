


using UnityEngine;



public class PlayerFPSController2 : MonoBehaviour
{
    private characterMovement characterMovement;
    private mouseLook mouseLook;
    [SerializeField] float walkSpeed = 5f;
    [SerializeField] float hRotationSpeed = 100f;
    [SerializeField] float vRotationSpeed = 80f;
    void Start()
    {
        GameObject.Find("DarthVader").gameObject.SetActive(false);

        Cursor.visible = false;

        Cursor.lockState = CursorLockMode.Locked;
        characterMovement = GetComponent<characterMovement>();
        mouseLook = GetComponent<mouseLook>();
    }
    void Update()
    {

        Movement();
        rotation();
    }

    private void Movement()
    {
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }

    private void rotation()
    {
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");

        mouseLook.HandleRotation(hRotationInput, vRotationInput);
    }
}
